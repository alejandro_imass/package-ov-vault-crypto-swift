// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.
import PackageDescription

let package = Package(
    name: "OVVaultCryptoSwift",
    platforms: [
        .macOS(.v10_15), .iOS(.v13),
    ],
    products: [
        .library(
            name: "OVVaultCryptoSwift",
            targets: ["OVVaultCryptoSwift"]),
    ],
    dependencies: [
        .package(url: "https://github.com/pengpengliu/BIP39.git", from: "1.0.1")
    ],
    targets: [
        .target(
            name: "OVVaultCryptoSwift",
            dependencies: ["BIP39"]),
        .testTarget(
            name: "OVVaultCryptoSwiftTests",
            dependencies: ["OVVaultCryptoSwift", "BIP39"]),
    ]
)
