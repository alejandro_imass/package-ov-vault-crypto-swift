import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(package_ov_vault_crypto_swiftTests.allTests),
    ]
}
#endif
