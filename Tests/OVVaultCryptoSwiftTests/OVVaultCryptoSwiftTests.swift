import Foundation
import XCTest
@testable import OVVaultCryptoSwift

final class OVVaultCryptoTests: XCTestCase {
    
    // use case 1a: Recovery Passphrase Words
    func testGeneratePassphraseWords() {
        let crypto = OVCrypto()
        let recoveryWords = try! crypto.generateRecoveryWords()
        print(recoveryWords)
        XCTAssertEqual(recoveryWords.count, 15)
    }

    // use case 1b: Get Seed from Recovery PPH
    func testGetSeedFromWords() {
        let crypto = OVCrypto()
        let recoveryWords = try! crypto.generateRecoveryWords()
        print(recoveryWords)
        XCTAssertEqual(recoveryWords.count, 15)
        let seed = try! crypto.getSeed(wordlist: recoveryWords);
        print(seed)
        XCTAssertEqual(seed.count, 64)
    }
    
    // use case 2: Derive DATA-KEY (using canonical BIP32 methods)
    func testGetDataKey() {
        let crypto = OVCrypto()
        let recoveryWords = try! crypto.generateRecoveryWords()
        print(recoveryWords)
        XCTAssertEqual(recoveryWords.count, 15)
        let seed = try! crypto.getSeed(wordlist: recoveryWords);
        print(seed)
        XCTAssertEqual(seed.count, 64)
        let dataKey = try! crypto.getDataKey(seed: seed)
        XCTAssertEqual(dataKey.count, 32)
        // these test vectors come from the reference Java library
        let testSeed: [UInt8] = [107, 64, 28, 67, 195, 100, 188, 174, 253, 160, 255, 152,
                        16, 222, 208, 13, 37, 136, 162, 130, 98, 74, 16, 217, 139,
                        151, 47, 130, 175, 13, 51, 112, 129, 245, 94, 7, 242, 55,
                        240, 111, 233, 42, 249, 158, 152, 176, 194, 227, 109, 28,
                        38, 200, 58, 82, 15, 178, 219, 119, 101, 225, 190, 24, 22, 186]
        let testKey: [UInt8] = [92, 96, 233, 51, 149, 49, 195, 211, 241, 208, 23, 84, 227, 108,
                       121, 71, 136, 30, 230, 125, 18, 160, 134, 137, 35, 32, 119, 38,
                       14, 125, 156, 206]
        let swiftDerived = try! crypto.getDataKey(seed: testSeed)
        print("BIP32 Derived Key from Java code:")
        print(testKey)
        print("BIP32 Derived Key from Swift code:")
        print(swiftDerived)
        XCTAssertEqual(Array(testKey), Array(swiftDerived))
        
    }

    // use cases 3, 4 and 7: Encrypt, Decrypt and Decrypt using Recovery Passphrase
    func testEncryptString (){
        // use case 1 and 2
        let crypto = OVCrypto()
        let recoveryWords = try! crypto.generateRecoveryWords()
        print(recoveryWords)
        XCTAssertEqual(recoveryWords.count, 15)
        let seed = try! crypto.getSeed(wordlist: recoveryWords);
        print(seed)
        XCTAssertEqual(seed.count, 64)
        let dataKey = try! crypto.getDataKey(seed: seed)
        XCTAssertEqual(dataKey.count, 32)
        // use case 3
        let sourceText = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÑκόσμε0123456789κόσμε";
        let encryptedBase64 = try! crypto.encryptString(data: sourceText, dataKey: dataKey);
        XCTAssertEqual(encryptedBase64.count, 132)
        // use case 4
        let decryptedString = try! crypto.decryptString(encDataStruct: encryptedBase64, dataKey: dataKey);
        XCTAssertEqual(sourceText,decryptedString)
        // use case 7 - re-derive root ket from words and decrypt data with that
        let derivedSeed = try! crypto.getSeed(wordlist: recoveryWords);
        print(seed)
        XCTAssertEqual(seed.count, 64)
        let derivedDataKey = try! crypto.getDataKey(seed: derivedSeed)
        let decryptedStringFromPPHDerivedKey = try! crypto.decryptString(encDataStruct: encryptedBase64, dataKey: derivedDataKey);
        XCTAssertEqual(sourceText,decryptedStringFromPPHDerivedKey)
        // these test vectors come from the reference Java library (using same source string above)
        let wordsFromJavaVault = ["alter", "melt", "globe", "torch", "lava", "enough", "table", "rebuild", "endless", "gate", "cat", "tuna", "quit", "material", "describe"]
        let javaEcnryptedStruct = "iEith280/QWBsdrOZtPtGYkusvVD5cF4+4do7rWO1mFIn7aZraikhx+IbsUwiDYAWwpmQlyJlhxbIvfwmKVwxvzUN0BbYizUqgV5PJ2DFu7bLyWCqp6nUiQRiML0C5iqMaw="
        let derivedSeedFromJavaLibWords = try! crypto.getSeed(wordlist: wordsFromJavaVault);
        let derivedDataKeyFromderivedSeedFromJavaLibWords = try! crypto.getDataKey(seed: derivedSeedFromJavaLibWords)
        let decryptedStringFromJavaWordsPPHDerivedKey = try! crypto.decryptString(encDataStruct: javaEcnryptedStruct, dataKey: derivedDataKeyFromderivedSeedFromJavaLibWords);
        // identical source text was used on both libs
        XCTAssertEqual(sourceText,decryptedStringFromJavaWordsPPHDerivedKey)

    }
        
    // unit test for PBKDF2_SHA256_GetHash against same pbkdf2 derivation in Java Reference lib
    func testPBKDF2_SHA256_GetHash(){
        let crypto = OVCrypto()
        let password = "A password with UTF-8 κόσμε and Punctuation Charactares !@#$%ˆ*()"
        let javaLibSalt: [UInt8] = [96, 78, 51, 200, 243, 32, 232, 103, 104, 61, 94, 149, 192, 4, 250, 91]
        let javaLibDerivedKey: [UInt8] = [77, 182, 198, 35, 196, 0, 125, 162, 148, 146, 200, 26, 99, 200, 131, 39, 139, 194, 157, 158, 89, 218, 30, 192, 62, 178, 32, 112, 76, 86, 28, 150]
        let derivedKey = crypto.PBKDF2_SHA256_GetHash(password: password, salt: javaLibSalt, iterations: 100000, keySizeBytes: 32)!
        print(derivedKey)
        XCTAssertEqual(javaLibDerivedKey,[UInt8](derivedKey))
    }
    
    // use cases 5,6 and 8
    func testEncryptDataKey() {
        // use case 1 and 2
        let crypto = OVCrypto()
        let recoveryWords = try! crypto.generateRecoveryWords()
        print(recoveryWords)
        XCTAssertEqual(recoveryWords.count, 15)
        let seed = try! crypto.getSeed(wordlist: recoveryWords);
        print(seed)
        XCTAssertEqual(seed.count, 64)
        let dataKey = try! crypto.getDataKey(seed: seed)
        XCTAssertEqual(dataKey.count, 32)
        // use case 3
        let sourceText = "ABCDEFGHIJKLMNOPQRSTUVWXYZÁÉÍÓÚÑκόσμε0123456789κόσμε";
        let encryptedBase64 = try! crypto.encryptString(data: sourceText, dataKey: dataKey);
        XCTAssertEqual(encryptedBase64.count, 132)
        // use case 4
        let decryptedString = try! crypto.decryptString(encDataStruct: encryptedBase64, dataKey: dataKey);
        XCTAssertEqual(sourceText,decryptedString)
        // use case 7 - re-derive root ket from words and decrypt data with that
        let derivedSeed = try! crypto.getSeed(wordlist: recoveryWords);
        print(seed)
        XCTAssertEqual(seed.count, 64)
        let derivedDataKey = try! crypto.getDataKey(seed: derivedSeed)
        let decryptedStringFromPPHDerivedKey = try! crypto.decryptString(encDataStruct: encryptedBase64, dataKey: derivedDataKey);
        XCTAssertEqual(sourceText,decryptedStringFromPPHDerivedKey)
        // use case 5 - encrypt data key
        let password = "A password with UTF-8 κόσμε and Punctuation Charactares !@#$%ˆ*()"
        let encryptedDataKey = try! crypto.encryptDataKey(dataKey: derivedDataKey, password: password)
        print("Encrypted Data Key:",encryptedDataKey)
        XCTAssertEqual(encryptedDataKey.count,108)
        // use case 6 - decrypt data key
        let decryptedDataKey = try! crypto.decryptDataKey(encDataKeyString: encryptedDataKey, password: password)
        XCTAssertEqual(decryptedDataKey,derivedDataKey)
        // this is a key litmus test: we should be able to decrypt an encrypted from the Java ref lib
        let javaDataKey: [UInt8] = [247, 191, 56, 9, 165, 149, 170, 233, 189, 182, 91, 150, 187, 5, 197, 227, 192, 241, 79, 167, 236, 211, 173, 173, 29, 49, 45, 97, 131, 145, 221, 153]
        let javaEncryptedDataKey = "ok/O4iblAhJAohoi3lmKAt3kylHzrNOojaeC++tn3y6KBOiLD8yhfZ9clNREEkQLLQKdtpW9Viq/zv0bFnz/kFpcHpI3L9Cqcfm+CyY8CnA="
        let decryptedDataKeyFromJava = try! crypto.decryptDataKey(encDataKeyString: javaEncryptedDataKey, password: password)
        XCTAssertEqual(decryptedDataKeyFromJava,javaDataKey)
        let newPassword = "A DIFFERENT password with UTF-8 κόσμε and Punctuation Charactares !@#$%ˆ*()";
        let encryptedDataKeyWithNewPassword = try! crypto.encryptDataKey(dataKey: decryptedDataKey, password: newPassword)
        print("Encrypted Data Key New Password:",encryptedDataKeyWithNewPassword)
        XCTAssertEqual(encryptedDataKeyWithNewPassword.count,108)
        let decryptedDataWithNewPassword = try! crypto.decryptDataKey(encDataKeyString: encryptedDataKeyWithNewPassword, password: newPassword)
        XCTAssertEqual(decryptedDataWithNewPassword,derivedDataKey)
        
    }

    
    static var allTests = [
        ("testGeneratePassphraseWords", testGeneratePassphraseWords),
        ("testGetSeedFromWords", testGetSeedFromWords),
        ("testGetDataKey", testGetDataKey),
    ]
}
