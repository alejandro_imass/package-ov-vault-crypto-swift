import XCTest

import package_ov_vault_crypto_swiftTests

var tests = [XCTestCaseEntry]()
tests += package_ov_vault_crypto_swiftTests.allTests()
XCTMain(tests)
