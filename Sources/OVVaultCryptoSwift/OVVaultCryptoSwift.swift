import Foundation
import BIP39
import CryptoKit
import CommonCrypto

public class OVCrypto {
    
    let IV_LENGTH_BYTES = 12;
    let ENTROPY_STRENGTH_BITS = 160
    let PBKDF2_DK_LENGTH_BYTES = 32;
    let PBKDF2_DK_ITERATIONS = 100000;
    let PBKDF2_SALT_LENGTH_BYTES = 32;
    
    public init() {
    }
    
    // use case 1a
    public func generateRecoveryWords() throws -> [String] {
        return Mnemonic(strength: ENTROPY_STRENGTH_BITS).phrase
    }
    
    // use case 1b
    public func getSeed(wordlist: [String]) throws -> [UInt8] {
        return try! Mnemonic(phrase: wordlist).seed
    }

    // use cases 2 and 7: derive root key (BIP32)
    public func getDataKey(seed: [UInt8]) throws -> [UInt8] {
        let key = SymmetricKey(data: "Bitcoin seed".data(using: .ascii)!)
        return Array(Array(HMAC<SHA512>.authenticationCode(for: seed, using: key))[0...31])
    }

    // use case 3: encrypt string data
    public func encryptString(data: String, dataKey: [UInt8]) throws -> String {
        let bytes = [UInt8](data.utf8)
        let key = SymmetricKey(data: dataKey)
        // Note: in Swift Crypto the IV is auto-generated and it uses the same
        // convention we use in the Java reference lib (IV+bytes)
        let sealedBox = try AES.GCM.seal(bytes, using: key)
        let cs = sealedBox.combined!
        return cs.base64EncodedString()
    }

    // use case 4: decrypt string data
    public func decryptString(encDataStruct: String, dataKey: [UInt8]) throws -> String {
        let bytes =  [UInt8](Data(base64Encoded: encDataStruct)!);
        let sealedBox = try AES.GCM.SealedBox(combined: bytes)
        let key = SymmetricKey(data: dataKey)
        // See note above: the IV is implicit in the sealedBox for Swift Crypto
        let decryptedBytes = try AES.GCM.open(sealedBox, using: key)
        return String(data: decryptedBytes, encoding: .utf8) ?? ""
    }

    // use case 5: encrypt data key
    enum RNGError: Error {
        case runtimeError(String)
    }
    public func encryptDataKey(dataKey: [UInt8], password: String) throws -> String {
        var salt = [UInt8](repeating: 0, count: PBKDF2_SALT_LENGTH_BYTES)
        let status = SecRandomCopyBytes(kSecRandomDefault, PBKDF2_SALT_LENGTH_BYTES, &salt)
        if status != errSecSuccess {
            throw RNGError.runtimeError("Secure RNG broken on this target")
        }
        let dk = PBKDF2_SHA256_GetHash(password: password, salt: salt, iterations: PBKDF2_DK_ITERATIONS, keySizeBytes: PBKDF2_DK_LENGTH_BYTES)!
        let iv = salt[0...IV_LENGTH_BYTES-1]
        let key = SymmetricKey(data: dk)
        // Note: here we need to by-pass the Swift Crypto auto-generated IV to comply
        // with the convention in the Java reference library
        let sealedBox = try AES.GCM.seal(dataKey, using: key, nonce: AES.GCM.Nonce(data: iv))
        let ct = sealedBox.ciphertext
        let tag = sealedBox.tag
        // See above. This is the same convention in the Java ref lib and the spec doc
        let encryptedDataKeyStruct = salt + ct + tag
        return String(data: Data(encryptedDataKeyStruct).base64EncodedData(), encoding: .ascii ) ?? ""
    }
    
    // use case 6: decrypt data key
    public func decryptDataKey(encDataKeyString: String, password: String) throws -> [UInt8]{
        // decode b64 and decompose parts. See notes above to understand this code
        let bytes = [UInt8](Data(base64Encoded: encDataKeyString)!);
        let salt: [UInt8] = Array(bytes[0...PBKDF2_SALT_LENGTH_BYTES-1])
        let ctAndTag: [UInt8] = Array(bytes[PBKDF2_SALT_LENGTH_BYTES...bytes.endIndex-1])
        let iv: [UInt8] = Array(salt[0...IV_LENGTH_BYTES-1])
        // Note: this is the convention used by Swift Crypto: iv + ct + tag
        let combined: [UInt8] = iv + ctAndTag
        // derive the encryption key
        let dk = PBKDF2_SHA256_GetHash(password: password, salt: salt, iterations: PBKDF2_DK_ITERATIONS, keySizeBytes: PBKDF2_DK_LENGTH_BYTES)!
        let key = SymmetricKey(data: dk)
        let sealedBox = try AES.GCM.SealedBox(combined: Data(bytes: combined, count: combined.count))
        let decryptedBytes = try AES.GCM.open(sealedBox, using: key)
        return [UInt8](decryptedBytes)
    }

    // utility methods
    
    public func PBKDF2_SHA256_GetHash(password: String, salt: [UInt8], iterations: Int, keySizeBytes: Int) -> [UInt8]? {
        guard let passwordData = password.data(using: .utf8) else { return nil }
        var derivedKeyData = Data(repeating: 0, count: keySizeBytes)
        let derivedCount = derivedKeyData.count
        let derivationStatus: Int32 = derivedKeyData.withUnsafeMutableBytes { derivedKeyBytes in
            let keyBuffer: UnsafeMutablePointer<UInt8> =
                derivedKeyBytes.baseAddress!.assumingMemoryBound(to: UInt8.self)
            return salt.withUnsafeBytes { saltBytes -> Int32 in
                let saltBuffer: UnsafePointer<UInt8> = saltBytes.baseAddress!.assumingMemoryBound(to: UInt8.self)
                return CCKeyDerivationPBKDF(
                    CCPBKDFAlgorithm(kCCPBKDF2),
                    password,
                    passwordData.count,
                    saltBuffer,
                    salt.count,
                    UInt32(kCCPRFHmacAlgSHA256),
                    //UInt32(kCMSEncoderDigestAlgorithmSHA256),
                    UInt32(iterations),
                    keyBuffer,
                    derivedCount)
            }
        }
        return derivationStatus == kCCSuccess ? [UInt8](derivedKeyData) : nil
    }
    
}
